//
//  PizzaPlaceDetailViewController
//  Spylight
//
//  Created by Michael Dihardja on 21/11/13.
//  Copyright (c) 2013 Spylight. All rights reserved.
//

#import "PizzaPlace.h"
#import <UIKit/UIKit.h>

@interface PizzaPlaceDetailViewController : UIViewController

@property (strong, nonatomic) IBOutlet PizzaPlace *thePizzaPlace;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;


@end
