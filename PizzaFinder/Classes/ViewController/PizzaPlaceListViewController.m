//
//  ViewController.m
//  PizzaFinder
//
//  Created by Michael Dihardja on 8/7/15.
//  Copyright (c) 2015 Michael Dihardja. All rights reserved.
//

#import "PizzaPlaceListViewController.h"
#import "PizzaListTableViewCell.h"
#import "PizzaPlace.h"
#import "ServerManager.h"
#import "LocationManager.h"
#import "PizzaPlaceDetailViewController.h"

@interface PizzaPlaceListViewController ()
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


@end

@implementation PizzaPlaceListViewController

@synthesize fetchedResultsController;

-(void)viewDidLoad{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"PizzaPlace"];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES]]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[TheAppDelegate managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsController setDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPizzaPlaces) name:K_LOCATION_UPDATED_NOTIFICATION object:nil];
   
    [self.tableView registerNib:[UINib nibWithNibName:[PizzaListTableViewCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[PizzaListTableViewCell reuseIdentifier]];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(refreshPizzaPlaces)
                  forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:self.refreshControl];
    
    TheLocationManager;
    [self refreshPizzaPlaces];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self reloadData];
}

-(void)reloadData{
   
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    [self.tableView reloadData];
}

-(void)refreshPizzaPlaces{
    [TheServerManager getNearbyPizzaPlacesWithLocation:[TheLocationManager currentLocation] andSuccessBlock:^(BOOL success) {
        [self reloadData];
    } andErrorBlock:^(NSError *err) {
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PizzaListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[PizzaListTableViewCell reuseIdentifier] forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(PizzaListTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    PizzaPlace *pizzaPlace = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setPizzaPlace:pizzaPlace];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PizzaPlace *pizzaPlace = [self.fetchedResultsController objectAtIndexPath:indexPath];
    PizzaPlaceDetailViewController *detailController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PizzaPlaceDetailViewController"];
    detailController.thePizzaPlace = pizzaPlace;
    [self.navigationController pushViewController:detailController animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
