//
//  ViewController.h
//  PizzaFinder
//
//  Created by Michael Dihardja on 8/7/15.
//  Copyright (c) 2015 Michael Dihardja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface PizzaPlaceListViewController : UITableViewController<NSFetchedResultsControllerDelegate>


@end

