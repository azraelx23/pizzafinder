//
//  PizzaPlaceDetailViewController
//  Spylight
//
//  Created by Michael Dihardja on 21/11/13.
//  Copyright (c) 2013 Spylight. All rights reserved.
//

#import "PizzaPlaceDetailViewController.h"

@interface PizzaPlaceDetailViewController ()

@end

@implementation PizzaPlaceDetailViewController
@synthesize thePizzaPlace,addressTextView;

#pragma mark - View Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = thePizzaPlace.name;
    self.addressTextView.text = thePizzaPlace.formattedAddress;
    
}

@end
