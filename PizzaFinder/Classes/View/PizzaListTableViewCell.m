//
//  ListTableViewCell.m
//  SeasideMarket
//
//  Created by Michael Dihardja on 7/11/12.
//  
//

#import "PizzaListTableViewCell.h"

@implementation PizzaListTableViewCell

@synthesize thePizzaPlace,nameLabel;

+ (NSString *)reuseIdentifier   {
    // return any identifier you like, in this case the class name
    return NSStringFromClass([self class]);
}

-(void)setPizzaPlace:(PizzaPlace *)pzPlace{
    self.thePizzaPlace = pzPlace;
    self.nameLabel.text = pzPlace.name;

}

@end
