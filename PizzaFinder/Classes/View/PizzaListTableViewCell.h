
//
//  ListTableViewCell.h
//  SeasideMarket
//
//  Created by Michael Dihardja on 7/11/12.
//  
//

#import <UIKit/UIKit.h>
#import "PizzaPlace.h"
#import "UIImageView+WebCache.h"

@interface PizzaListTableViewCell : UITableViewCell

@property(nonatomic, retain) PizzaPlace *thePizzaPlace;
@property(nonatomic, retain) IBOutlet UILabel *nameLabel;

+ (NSString *)reuseIdentifier;
-(void)setPizzaPlace:(PizzaPlace *)pzPlace;

@end
