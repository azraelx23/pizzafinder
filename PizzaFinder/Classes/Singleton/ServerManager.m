 //
//  SettingsManager.m
//  Guestbook
//
//  Created by Michael Dihardja
//  Copyright 2009 MD Company. All rights reserved.
//

#import "ServerManager.h"

static NSString * const SERVER_URL_STRING = @"https://api.foursquare.com/v2/";
static NSString * const FS2_OAUTH_KEY = @"5P1OVCFK0CCVCQ5GBBCWRFGUVNX5R4WGKHL2DGJGZ32FDFKT";
static NSString * const FS2_OAUTH_SECRET = @"UPZJO0A0XL44IHCD1KQBMAYGCZ45Z03BORJZZJXELPWHPSAR";


@implementation ServerManager

@synthesize defaults,  queue;

+ (id)sharedManager {
    static ServerManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(id)init {
	if (self = [super init]) {
		self.defaults = [NSUserDefaults standardUserDefaults];
        queue = [[NSOperationQueue alloc] init];
        queue.maxConcurrentOperationCount = 1;
        
        //[[AFNetworkActivityLogger sharedLogger] startLogging];
        //[[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
	}
	return self;
}

-(AFHTTPRequestOperation *)operationWithURLString:(NSString *)urlString{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    return operation;
}


#pragma mark PIZZA PLACES
- (void)getNearbyPizzaPlacesWithLocation:(CLLocation *)location
                         andSuccessBlock:(void(^)(BOOL success))completionBlock
                           andErrorBlock:(void(^)(NSError *err))errBlock
{
    
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@venues/explore",SERVER_URL_STRING];
    [urlString appendFormat:@"?client_id=%@",FS2_OAUTH_KEY];
    [urlString appendFormat:@"&client_secret=%@",FS2_OAUTH_SECRET];
    [urlString appendString:@"&v=20140806"];
    [urlString appendString:@"&m=foursquare"];
    [urlString appendString:@"&radius=250"];
    [urlString appendString:@"&section=food"];
    [urlString appendString:@"&query=pizza"];
    [urlString appendString:@"&limit=5"];
    [urlString appendString:@"&venuePhotos=1"];
    [urlString appendString:@"&sortByDistance=1"];
    [urlString appendFormat:@"&ll=%f,%f",location.coordinate.latitude,location.coordinate.longitude];
    AFHTTPRequestOperation *operation = [self operationWithURLString:urlString];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *op, id JSON) {
        NSDictionary *jsonDictionary = (NSDictionary *)JSON;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if(jsonDictionary[@"response"] && jsonDictionary[@"response"][@"groups"]){
                
                
                NSArray *groupArray = jsonDictionary[@"response"][@"groups"];
                
                if(groupArray && groupArray.count>0){
                    NSDictionary *groupDictionary = groupArray[0];
                    if(groupDictionary && groupDictionary[@"items"]){
                        NSArray *itemsArray = groupDictionary[@"items"];
                        if(itemsArray.count>0){
                            [TheAppDelegate clearData];
                            
                            NSManagedObjectContext *context = [TheAppDelegate managedObjectContext];
                            for(NSDictionary *itemDictionary in itemsArray){
                                NSDictionary *venueDictionary = itemDictionary[@"venue"];
                                
                                _PizzaPlace *pizzaPlace = [_PizzaPlace insertInManagedObjectContext:context];
                                pizzaPlace.theId = venueDictionary[@"id"];
                                pizzaPlace.name = venueDictionary[@"name"];
                                if(venueDictionary[@"location"] && venueDictionary[@"location"][@"formattedAddress"]){
                                    NSArray *addressArray = venueDictionary[@"location"][@"formattedAddress"];
                                    NSMutableString *addressString = [NSMutableString string];
                                    BOOL isFirstLine = YES;
                                    for(NSString *str in addressArray){
                                        if(!isFirstLine){
                                            [addressString appendString:@"\n"];
                                        }
                                        [addressString appendString:str];
                                        isFirstLine = NO;
                                    }
                                    pizzaPlace.formattedAddress = addressString;
                                    pizzaPlace.distance = venueDictionary[@"location"][@"distance"];
                                }
                            }
                            [context save:nil];
                        }
                    }
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(YES);
            });
        });
        
        
        
        
    } failure:^(AFHTTPRequestOperation *op, NSError *error) {
        errBlock(error);
    }];
    [queue addOperation:operation];
}





@end
