#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define TheLocationManager ([LocationManager sharedManager])

@interface LocationManager : NSObject <CLLocationManagerDelegate>{
    
}
@property(nonatomic,assign) BOOL locationAvailable;

+(id)sharedManager;
-(CLLocation *)currentLocation;
-(BOOL)isLocationServiceAvailable;

@end