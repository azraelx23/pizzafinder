//
//  AppDelegate.h
//  PizzaFinder
//
//  Created by Michael Dihardja on 8/7/15.
//  Copyright (c) 2015 Michael Dihardja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

static NSString * const K_LOCATION_UPDATED_NOTIFICATION = @"K_LOCATION_UPDATED_NOTIFICATION";

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)clearData;

#define TheAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]


@end

