//
//  SettingsManager.h
//  Guestbook
//
//  Created by Michael Dihardja on 8/17/09.
//  Copyright 2009 MD Company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "_PizzaPlace.h"
#import "PizzaPlace.h"

#import <CoreLocation/CoreLocation.h>

#define TheServerManager ([ServerManager sharedManager])

@interface ServerManager : NSObject {
    
}

@property (nonatomic, strong) NSUserDefaults *defaults;
@property (nonatomic, strong) NSOperationQueue *queue;

+ (id)sharedManager;
- (void)getNearbyPizzaPlacesWithLocation:(CLLocation *)location
                         andSuccessBlock:(void(^)(BOOL success))completionBlock
                           andErrorBlock:(void(^)(NSError *err))errBlock;

@end
