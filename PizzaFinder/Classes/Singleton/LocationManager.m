#import "LocationManager.h"
#import "AppDelegate.h"

@implementation LocationManager{
    CLLocationManager *locationManager;
    
}

@synthesize locationAvailable;

+ (id)sharedManager {
    static LocationManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        locationManager.distanceFilter = 100.0f;
        [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
    }
    return self;
}




#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.locationAvailable = YES;
    //CLLocation *newLocation = [locations lastObject];
    [[NSNotificationCenter defaultCenter] postNotificationName:K_LOCATION_UPDATED_NOTIFICATION object:nil];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //TODO: handle error
    self.locationAvailable = NO;
}


#pragma mark - Helpers

-(BOOL)isLocationServiceAvailable
{
    if([CLLocationManager locationServicesEnabled]==NO ||
       [CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied ||
       [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted){
        return NO;
    }else{
        return YES;
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"CLLocationAuthorization status: %d",status);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didChangeAuthorizationStatus" object:nil userInfo:@{@"kStatus": @(status)}];
}

-(CLLocation *)currentLocation{
    return locationManager.location;
}

@end