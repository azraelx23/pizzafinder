// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PizzaPlace.m instead.

#import "_PizzaPlace.h"

const struct PizzaPlaceAttributes PizzaPlaceAttributes = {
	.distance = @"distance",
	.formattedAddress = @"formattedAddress",
	.name = @"name",
	.priceTier = @"priceTier",
	.theId = @"theId",
};

const struct PizzaPlaceUserInfo PizzaPlaceUserInfo = {
	.MMRecordEntityPrimaryAttributeKey = @"id",
};

@implementation PizzaPlaceID
@end

@implementation _PizzaPlace

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PizzaPlace" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PizzaPlace";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PizzaPlace" inManagedObjectContext:moc_];
}

- (PizzaPlaceID*)objectID {
	return (PizzaPlaceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"distanceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"distance"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic distance;

- (int32_t)distanceValue {
	NSNumber *result = [self distance];
	return [result intValue];
}

- (void)setDistanceValue:(int32_t)value_ {
	[self setDistance:@(value_)];
}

- (int32_t)primitiveDistanceValue {
	NSNumber *result = [self primitiveDistance];
	return [result intValue];
}

- (void)setPrimitiveDistanceValue:(int32_t)value_ {
	[self setPrimitiveDistance:@(value_)];
}

@dynamic formattedAddress;

@dynamic name;

@dynamic priceTier;

@dynamic theId;

@end

