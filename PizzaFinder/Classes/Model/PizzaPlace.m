//
//  PizzaPlace.m
//  
//
//  Created by Michael Dihardja on 8/7/15.
//
//

#import "PizzaPlace.h"


@implementation PizzaPlace

@dynamic theId;
@dynamic name;
@dynamic formattedAddress;
@dynamic distance;

@end
