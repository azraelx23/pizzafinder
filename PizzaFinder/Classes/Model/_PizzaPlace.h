// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PizzaPlace.h instead.

@import CoreData;

extern const struct PizzaPlaceAttributes {
	__unsafe_unretained NSString *distance;
	__unsafe_unretained NSString *formattedAddress;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *priceTier;
	__unsafe_unretained NSString *theId;
} PizzaPlaceAttributes;

extern const struct PizzaPlaceUserInfo {
	__unsafe_unretained NSString *MMRecordEntityPrimaryAttributeKey;
} PizzaPlaceUserInfo;

@interface PizzaPlaceID : NSManagedObjectID {}
@end

@interface _PizzaPlace : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PizzaPlaceID* objectID;

@property (nonatomic, strong) NSNumber* distance;

@property (atomic) int32_t distanceValue;
- (int32_t)distanceValue;
- (void)setDistanceValue:(int32_t)value_;

//- (BOOL)validateDistance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* formattedAddress;

//- (BOOL)validateFormattedAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* priceTier;

//- (BOOL)validatePriceTier:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* theId;

//- (BOOL)validateTheId:(id*)value_ error:(NSError**)error_;

@end

@interface _PizzaPlace (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveDistance;
- (void)setPrimitiveDistance:(NSNumber*)value;

- (int32_t)primitiveDistanceValue;
- (void)setPrimitiveDistanceValue:(int32_t)value_;

- (NSString*)primitiveFormattedAddress;
- (void)setPrimitiveFormattedAddress:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitivePriceTier;
- (void)setPrimitivePriceTier:(NSString*)value;

- (NSString*)primitiveTheId;
- (void)setPrimitiveTheId:(NSString*)value;

@end
