//
//  PizzaPlace.h
//  
//
//  Created by Michael Dihardja on 8/7/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PizzaPlace : NSManagedObject

@property (nonatomic, retain) NSString * theId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * formattedAddress;
@property (nonatomic, retain) NSNumber * distance;

@end
