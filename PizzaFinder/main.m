//
//  main.m
//  PizzaFinder
//
//  Created by Michael Dihardja on 8/7/15.
//  Copyright (c) 2015 Michael Dihardja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
